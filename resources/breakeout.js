//game state
let gemeOnPause = false;
let gameOver = true;
let userIsLogined = false;
let score = 0;
let bestScore = 0;

//board
let board;
const boardWidth = 500;
const boardHeight = 500;
let context;


//player
let userName;
const playerWidth = boardWidth/5.5;
const playerHeight = boardHeight/50;
const playerVelocity = boardHeight/50;

//ball
const ballWidth = 10;
const ballHeight = 10;
const ballVelocityX = 2;
const ballVelocityY = 3;

//brick
const brickWidth  = 50
const brickHeight = 20

let defaultBricks = [{x: 15, y : 50}, {x : 75, y: 50}, {x: 135, y: 50}, {x: 195, y: 50}, {x: 255, y: 50},{x: 315, y : 50}, {x : 375, y: 50}, {x: 435, y: 50},
    {x: 15, y : 80}, {x : 75, y: 80}, {x: 135, y: 80}, {x: 195, y: 80}, {x: 255, y: 80},{x: 315, y : 80}, {x : 375, y: 80}, {x: 435, y: 80},
    {x: 15, y : 110}, {x : 75, y: 110}, {x: 135, y: 110}, {x: 195, y: 110}, {x: 255, y: 110},{x: 315, y : 110}, {x : 375, y: 110}, {x: 435, y: 110},
    {x: 15, y : 140}, {x : 75, y: 140}, {x: 135, y: 140}, {x: 195, y: 140}, {x: 255, y: 140},{x: 315, y : 140}, {x : 375, y: 140}, {x: 435, y: 140},
    {x: 15, y : 170}, {x : 75, y: 170}, {x: 135, y: 170}, {x: 195, y: 170}, {x: 255, y: 170},{x: 315, y : 170}, {x : 375, y: 170}, {x: 435, y: 170}
];

// let defaultBricks = [];
let bricks = [];
for (var i = 0; i < defaultBricks.length; i++) {
    bricks.push({x: defaultBricks[i].x, y: defaultBricks[i].y});
}


const brickTouch = new Audio('resources/audio/bricktouch.mp3');
const winSound = new Audio('resources/audio/winSound.mp3');
const loseSound = new Audio('resources/audio/loseSound.mp3');


function drawBricks(){
    for(let i = 0; i < bricks.length; i++){
        context.fillStyle = "lightgreen";
        context.fillRect(bricks[i].x, bricks[i].y, brickWidth, brickHeight);
    }
}

// class Clock is responsible for calculate time
class Clock{
    stopwatchInterval;
    elapsedTime;

    //The largest number of milliseconds in game
    maxTime;


    constructor() {
        this.elapsedTime = 0;
        this.maxTime = 0;
    }

    pad(unit) {
        return (`0` + unit).slice(-2);
    }
    startStopwatch() {
        const stopwatchElement = document.querySelector('#stopWatch');
        let startTime = Date.now() - this.elapsedTime;

        this.stopwatchInterval = setInterval(e => {
            this.elapsedTime = Date.now() - startTime;
            if(this.elapsedTime > this.maxTime){
                this.maxTime = this.elapsedTime;
            }
            let seconds = Math.floor((this.elapsedTime / 1000) % 60);
            let minutes = Math.floor((this.elapsedTime / (1000 * 60)) % 60);
            let hours = Math.floor((this.elapsedTime / (1000 * 60 * 60)) % 24);

            seconds = this.pad(seconds);
            minutes = this.pad(minutes);
            hours = this.pad(hours);

            stopwatchElement.textContent = `Current time: ${hours}:${minutes}:${seconds}`;
            this.drawMaxTime()
    }, 1000);
    }

    stopStopwatch() {
        clearInterval(this.stopwatchInterval);
        this.elapsedTime = 0;
    }


    
    drawMaxTime(){
        const maxTimeElement = document.querySelector('#maxTime');
        let seconds = Math.floor((this.maxTime / 1000) % 60);
        let minutes = Math.floor((this.maxTime / (1000 * 60)) % 60);
        let hours = Math.floor((this.maxTime / (1000 * 60 * 60)) % 24);

        seconds = this.pad(seconds);
        minutes = this.pad(minutes);
        hours = this.pad(hours);

        maxTimeElement.textContent = `Your Record: ${hours}:${minutes}:${seconds}`;
    }

    resetStopwatch() {
        const stopwatchElement = document.getElementById('stopwatch');
        stopwatchElement.textContent = '00:00:00';
        this.elapsedTime = 0;
    }
    

}

// class Drawable responsible to draw elements on canvas
class Drawable{
    width;
    height;
    x;
    y;
    fillStyle;
    constructor(width, height, x, y){
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }
    draw(){
        context.fillStyle = this.fillStyle;
        context.fillRect(this.x, this.y, this.width, this.height);
    }
}

//class Ball is responsible to calculate ball logic
class Ball extends Drawable{
    constructor(ballWidth, ballHeight, ballVelocityX, ballVelocityY) {
        super(ballWidth, ballHeight, boardWidth/2, boardHeight/2)
        this.velocityX = ballVelocityX;
        this.velocityY = ballVelocityY;
        this.fillStyle = "white";
    }

    update(playerX, playerY) {
        this.x += this.velocityX;
        this.y += this.velocityY;
        if(this.x + this.width > boardWidth || this.x < 0){
            this.velocityX = -this.velocityX;
        }
        if(this.y <= 0){
            this.velocityY = -this.velocityY;
        }
        if(this.y + this.height >= playerY && this.y + this.height <= playerY + player.height/2 && this.x < playerX + playerWidth && this.x > playerX){
            if(this.velocityY > 0){
                this.velocityY = -this.velocityY;
            }
        }

        if(this.y + this.height > boardHeight){
            gameOver = true;
        }
    }

}


//class Player is responsible for calculate player logic
class Player extends Drawable{
    
    
    constructor(playerWidth, playerHeight, boardWidth, boardHeight, playerVelocity) {
        super(playerWidth, playerHeight, boardWidth/2 - playerWidth/2, boardHeight - playerHeight - 5);
        this.velocity = playerVelocity; 
        this.fillStyle = "skyblue";
    }

    moveLeft(){
        if(this.x < 5)
            return;
        this.x -= this.velocity
    }

    moveRight(){
        if(this.x > boardWidth - this.width - 5)
            return;
        this.x += this.velocity
    }

}

//class witch responsible for handle drag and drop logic
class DragAndDrop{
    draggable1;
    draggable2;

    constructor(){
        this.draggable1 = document.querySelector("#userInfo");
        this.imageIsSet = false;    
        this.draggable1.addEventListener("dragstart", e => {
            e.dataTransfer.setData("text/plain", this.draggable1.id);
        });


        this.draggable2 = document.querySelector("#Time");
        this.draggable2.addEventListener("dragstart", e => {
            e.dataTransfer.setData("text/plain", this.draggable2.id);
        });

        this.dropArea = document.getElementById("dropZoneUserImage");
        this.inputFile = document.getElementById("userImage");
    }

    dragAndDropListener(){
        for(const dropZone of document.querySelectorAll(".dropZone")){
            dropZone.addEventListener("dragover", e => {
                e.preventDefault();
                dropZone.classList.add("dropZone--over");
            });
            
            dropZone.addEventListener("drop", e => {
                e.preventDefault();
                if(dropZone.classList.contains("dropZoneWithItem")){
                    dropZone.classList.remove("dropZone--over");
                    return;
                }
                if(e.dataTransfer.files.length!= 0){
                    if(this.imageIsSet){
                        dropZone.classList.remove("dropZone--over");
                        return;
                    }
                    this.imageIsSet = true;
                    document.getElementById("userImage").files = e.dataTransfer.files;
                    let imgLink = URL.createObjectURL(this.inputFile.files[0]);
                    this.dropArea.style.backgroundImage = `url(${imgLink})`
                    dropZone.classList.remove("dropZone--over");
                    let p = dropZone.querySelector("p");
                    dropZone.removeChild(p);
                    dropZone.classList.add("dropZoneWithItem")
                    return;
                }
                const id = e.dataTransfer.getData("text/plain");
                const draggableElement = document.getElementById(id);
                dropZone.appendChild(draggableElement);
            
                dropZone.classList.remove("dropZone--over");
            });

            dropZone.addEventListener("dragleave", e => {
                dropZone.classList.remove("dropZone--over");
            });
           

        }
    
    }
}



let mouseCoordsPrev = 0;


//initialize the instances of game objects
let player = new Player(playerWidth, playerHeight, boardWidth, boardHeight, playerVelocity);
let ball = new Ball(ballWidth, ballHeight, ballVelocityX, ballVelocityY);
let clock = new Clock();


function gameToStart(){
    ball = new Ball(ballWidth, ballHeight, ballVelocityX, ballVelocityY);
    gameOver = false;
}

// variable which responsible for pop up menu
let modalVisible = false;


//application start here
window.onload = e =>{
    
    let dragAndDrop = new DragAndDrop();
    dragAndDrop.dragAndDropListener();
    
    let userInformation = document.querySelector('#userInformation');
    userInformation.addEventListener("mousedown", e => {
        startX = e.clientX;
        startY = e.clientY;
        document.addEventListener("mousemove", mouseMove);
        document.addEventListener("mouseup", mouseUp);
    })


    //drawing pop up menu and validate user input
    inputValidation();


    const modalButtons = document.querySelectorAll('#nameSubmit');
    const pageWrapper = document.querySelectorAll('.page-wrapper');
    const modal = document.querySelector('.modal');
    

    board = document.querySelector('#board');
    board.width = boardWidth;
    board.height = boardHeight;
    
    context = board.getContext('2d'); 
    context.fillStyle = "lightblue";
    context.fillRect(player.x, player.y, player.width, player.height);

    gameStart()
    document.addEventListener('keydown', e => {
        if(userIsLogined){
            if(e.code == 'KeyR'){
                gameOnPause = !gameOnPause;
                clock.drawMaxTime();
                if(gameOver){
                    clock.startStopwatch();
                }
                gameToStart();
            }
        }
    })

    //change players board position based on current mouse position
    document.addEventListener('mousemove', e =>{
        newCoords = (e.clientX - (window.innerWidth - boardWidth)/2 - player.width/2);
        
        if(e.clientX < window.innerWidth/2 - boardWidth/2) 
            return;

        if(player.x > boardWidth - player.width && e.clientX > window.innerWidth/2 + boardWidth/2)
            return;

        player.x = newCoords;
    })
    
    requestAnimationFrame(update)
}

//function update is responsible for updating the game state every frame
function update(){
    if(!gameOnPause){
        scoreUpdate();
        if(!gameOver){
            if(bricks.length == 0){
                printOnBoard("You won", "Press R to restart");
                winSound.play();
                updateBestScore();
                gameOnPause = true;
                gameOver = true;
                clock.stopStopwatch();
            }
            else{
                context.clearRect(0,0, boardWidth, boardHeight)
                context.fillStyle = "lightblue";
                player.draw()
                ball.update(player.x, player.y);
                for(br in bricks){
                    if(topCollision(ball,  bricks[br])){
                        ball.velocityY = -ball.velocityY;
                    }
                    if(bottomCollision(ball,  bricks[br])){
                        ball.velocityY = -ball.velocityY;
                    }
                    if(leftCollision(ball,  bricks[br])){
                        ball.velocityX = -ball.velocityX;
                    }
                    if(rightCollision(ball,  bricks[br])){
                        ball.velocityX = -ball.velocityX;
                    }
                    if(detectCollision(ball, bricks[br])){
                        brickTouch.play()
                        bricks.splice(br, 1);
                        score = score + 1;
                    }
                }
                ball.draw();
                drawBricks();
            }
            
        }else{
            printOnBoard("Game Over", "Press R to restart")
            loseSound.play();
            updateBestScore();
            bricks.splice(0, bricks.length)
            score = 0;
            clock.stopStopwatch();
            gameOnPause = true;
            gameOver = true;
            for (var i = 0; i < defaultBricks.length; i++) {
                bricks.push({x: defaultBricks[i].x, y: defaultBricks[i].y});
            }
            }
        

    
    }
    requestAnimationFrame(update);
}

//update Best Score function
function updateBestScore(){
    if(bestScore< score){
        bestScore = score;
    }
}

//draw menu on the canvas with elements
function printOnBoard(firstMessage, secondMessage){
    context.clearRect(0,0, boardWidth, boardHeight)
    context.fillStyle = "black";
    context.fillRect(0,0, boardWidth, boardHeight)
    context.fillStyle = "white";
    context.font = "25px Arial";
    context.fillText(firstMessage, boardWidth/2 - 120, boardHeight/2 - 50)
    context.fillText(secondMessage, boardWidth/2 - 120, boardHeight/2)
    context.fillText("Your score is: " + score, boardWidth/2 - 120, boardHeight/2 + 50)
}


function gameStart(){
    gameOnPause = true;
    printOnBoard("Game on pause", "Press R to start the game")
}

//Next functions are responsible for detect collision between two objects
function detectCollision(a, b){
    if(a.x < b.x + brickWidth && a.x + a.width > b.x && a.y < b.y + brickHeight && a.y + a.height > b.y){
        return true;
    }else{
        return false;
    }
}

function topCollision(myBall, myBrick){
    return detectCollision(myBall, myBrick) && (myBall.y + myBall.height) >= myBrick.y + ballVelocityY + 1; 
}


function bottomCollision(myBall, myBrick){
    return detectCollision(myBall, myBrick) && (myBrick.y + brickHeight - ballVelocityY - 1) >= myBall.y;
}

function leftCollision(myBall, myBrick){
    return detectCollision(myBall, myBrick) && (myBall.x + myBall.width) >= myBrick.x +ballVelocityX + 1;
}

function rightCollision(myBall, myBrick){
    return detectCollision(myBall, myBrick) && (myBrick.x + brickWidth - ballVelocityX - 1) >= myBall.x;
}



function toggleModalState () {
    modalVisible = !modalVisible;
    if (modalVisible) {
        document.body.classList.add('modal-visible');
    } else {
        document.body.classList.remove('modal-visible');
    }
}


//validate input from user on the start popup menu
function inputValidation(){
    toggleModalState();
    submit = document.querySelector("#nameSubmit")
    submit.addEventListener("click", e => {
        playerName = document.querySelector("#name")
        if(playerName.value == ""){
            document.querySelector("#message").innerHTML = "Please enter a name";
            e.preventDefault();
            return;
        }if(playerName.value.length < 3){
            document.querySelector("#message").innerHTML = "Name must be at least 3 characters";
        }else{
            userName = playerName.value;
            setUserName();
            userIsLogined = true;
            toggleModalState()
        }
        e.preventDefault();
    })
}


function setUserName(){
    document.querySelector("#userName").innerHTML ="Name: " + userName;
}

function scoreUpdate(){
    bestScoreTag = document.querySelector("#bestScore");
    bestScoreTag.innerHTML = "Best Score: " + bestScore;
    scoreTag = document.querySelector("#score");
    scoreTag.innerHTML = "Current Score: " + score;
}

let newX = 0;
let newY = 0;
let startX = 0;
let startY = 0;



function mouseMove(e){
    newX = startX - e.clientX;
    newY = startY - e.clientY;
    
    startX = e.clientX;
    startY = e.clientY;

    userInformation.style.top = (userInformation.offsetTop - newY) + "px";
    userInformation.style.left = (userInformation.offsetLeft - newX) + "px";


}

function mouseUp(e){
    document.removeEventListener("mousemove", mouseMove);
}


