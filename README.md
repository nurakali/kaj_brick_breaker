## Dokumentace projektu BreakOut
### Cíl projektu
Cílem tohoto projektu je vytvořit webovou verzi klasické hry BreakOut, která byla původně
populární na platformě Atari. Hra je určena pro zábavu a zlepšení koordinačních schopností
hráčů prostřednictvím interaktivního a vizuálně přitažlivého rozhraní. Uživatelé mohou
ovládat plošinu, odrážet míček a ničit cihly pro získání bodů.
### Popis funkcionalit
#### Projekt BreakOut obsahuje následující klíčové funkcionality:
* Dynamické ovládání plošiny: Uživatelé mohou ovládat plošinu pohybem myši, což
umožňuje odrážet míček zpět k cihlám.
* Detekce kolizí: Systém automaticky detekuje kolize mezi míčkem a cihlami nebo
okraji herní plochy. Při kontaktu míček mění směr.
* Skórování a záznam nejlepšího výsledku: Hra zaznamenává skóre během hry a
ukládá nejlepší dosažené skóre.
* Zvukové efekty: Pro zvýšení zážitku jsou při různých událostech (např. při zásahu
cihly) přehrávány zvukové efekty.
* Grafické rozhraní: Grafické rozhraní obsahuje pohled na herní plochu, plošinu,
míček a cihly.
### Postup řešení
#### Projekt je rozdělen do několika hlavních částí:
* Inicializace herního plánu: Nastavení rozměrů herní plochy a počátečních pozic
objektů (plošiny, míčku, cihel).
* Správa událostí: Ovládání plošiny pomocí myši nebo klávesnice a reakce na kolize
s míčkem.
* Vykreslování objektů: Pravidelné vykreslování pohybu plošiny, míčku a změn na
herním plánu.
* Zvukové efekty: Implementace audio prvků pro zvýšení interaktivnosti hry.
* Ukládání a zobrazování skóre: Sledování a zobrazování skóre během hry a po
jejím skončení.
 
 V rámci těchto částí je kód organizován do tříd a funkcí, které společně definují chování a
vizuální reprezentaci herních prvků. Každý herní objekt, jako jsou plošina, míček a cihly, je
reprezentován třídou s metodami pro jejich vykreslení a aktualizaci stavu.